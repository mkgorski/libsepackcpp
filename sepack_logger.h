#ifndef SEPACK_LOGGER_H
#define SEPACK_LOGGER_H

#include <map>

class SepackLogger {
  public:
    SepackLogger() : mIdx(0)
    {
        parseDBfile();
    }

    void process(uint8_t *data, uint8_t len);

  private:
    int tryProcessPacket(uint32_t *data, uint8_t len);
    void parseDBfile();

    uint8_t mBuffer[1024];
    int mIdx;
    std::map<uint32_t, std::string> messages;
};

#endif /* SEPACK_LOGGER_H */