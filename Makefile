# Tool definitions
CC      = gcc
CXX		= g++
LD      = ld
AR      = ar
AS      = as
CP      = objcopy
OD      = objdump
SIZE    = size
RM      = rm
PAGER  ?= less
Q       = @./quiet "$@"

# Flags
CXXFLAGS   = -W -Wall -Os -fPIC
ASFLAGS  =
CPFLAGS  =
ODFLAGS  = -x --syms
PRFLAGS ?=
LDLIBS   = -lusb-1.0

# Source files
HEADERS = sepack.h sepack_logger.h
CSRCS   = cJSON.c
CPPSRCS = sepack.cpp sepack_logger.cpp
ASRCS  = 

# Path handling
OUTDIR := build
TOBJS = $(CSRCS:.c=.o) $(CPPSRCS:.cpp=.o) $(ASRCS:.s=.o)
OBJS = $(addprefix $(OUTDIR)/, $(TOBJS))

# OS X and Linux support
UNAME := $(shell uname -s)
ifeq ($(UNAME),Linux)
	LDFLAGS_SHARED  = -shared
	BIN_SHARED = libsepackcpp.so
else
ifeq ($(UNAME),Darwin)
	LDFLAGS_SHARED  = -dynamiclib
	BIN_SHARED = libsepackcpp.dylib
else
	error
endif
endif

LDFLAGS_STATIC  = rcs
BIN_STATIC = libsepackcpp.a


.PHONY: all static shared install clean nuke

all: $(BIN_SHARED) $(BIN_STATIC)

$(BIN_SHARED): $(OBJS)
	$Q $(CXX) $(LDFLAGS_SHARED) $^ $(LDLIBS) -o $@

$(BIN_STATIC): $(OBJS)
	$Q $(AR) $(LDFLAGS_STATIC) $@  $^

$(OUTDIR)/%.o: %.cpp
	@$(CXX) $(CXXFLAGS) -MM $< -MF $(OUTDIR)/$*.d -MP -MT "$@"
	$Q $(CXX) -c $(CXXFLAGS) $< -o $@

$(OUTDIR)/%.o: %.c
	@$(CXX) $(CXXFLAGS) -MM $< -MF $(OUTDIR)/$*.d -MP -MT "$@"
	$Q $(CXX) -c $(CXXFLAGS) $< -o $@

$(OBJS): | $(OUTDIR)

$(OUTDIR):
	@mkdir -p $(OUTDIR)

install: all
	cp $(BIN_STATIC) $(BIN_SHARED) /usr/local/lib
	cp $(HEADERS) /usr/local/include


clean:
	@-rm -rf $(OUTDIR)/*

nuke: clean
	-rm -f $(BIN_STATIC) $(BIN_SHARED)

-include $(addprefix $(OUTDIR)/,$(notdir $(CSRCS:.c=.d)))
