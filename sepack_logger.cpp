#include <cstdio>
#include <cstdlib>
#include <cstring>
#include "sepack.h"
#include "sepack_logger.h"
#include "cJSON.h"

#define BASH_RESET  "\e[0m"
#define BASH_YELLOW "\e[33m"
#define BASH_CYAN   "\e[96m"

int
SepackLogger::tryProcessPacket(uint32_t *data, uint8_t len)
{
    // static uint8_t lost_messages = 0;
    std::map<uint32_t, std::string>::iterator it;
    static uint8_t lostMsg = 0;
    
    uint32_t *b = data;
    // is there enough space for mandatory (status, logid, ts) fields?
    if (len < 12)
        return -1;

    uint32_t status = *b++;
    uint8_t lost = status & 0xff;
    uint32_t logid = *b++;
    int nargs =  (*b >> 24);
    uint32_t ts = (*b++ & 0xffffff);
    len -= 12;

    if (lostMsg != lost) {
        int sec = ts / 1000;
        int msec = ts % 1000;
        printf(BASH_YELLOW "[%d.%03d] " BASH_CYAN "Lost %d debug messages\n" BASH_RESET, 
               sec, msec, (uint8_t)(lost - lostMsg));
        lostMsg = lost;
    }
    // printf("status: 0x%x, logid: 0x%x, nargs: %d, ts: %d\n", status, logid, nargs, ts);

    //sanity check
    if (nargs > 4) {
        return 1;
    }
    // is there space for arguments?
    if (len < nargs * 4)
        return -2;

    it = messages.find(logid);
    if (it == messages.end()) {
        printf("Message with log id 0x%08x not found in database\n", logid);
        return 1;
    }
    else {
        std::string msg = std::string(BASH_YELLOW "[%d.%03d] " BASH_CYAN) + it->second + BASH_RESET + "\n";
        int sec = ts / 1000;
        int msec = ts % 1000;
        switch (nargs) {
            case 0: printf(msg.c_str(), sec, msec); break;
            case 1: printf(msg.c_str(), sec, msec, b[0]); break;
            case 2: printf(msg.c_str(), sec, msec, b[0], b[1]); break;
            case 3: printf(msg.c_str(), sec, msec, b[0], b[1], b[2]); break;
            case 4: printf(msg.c_str(), sec, msec, b[0], b[1], b[2], b[3]); break;
            default: printf("more than 4 arguments not supported\n"); break;
        }    
    }

    return 12 + nargs * 4;
}

void printfbuf(uint8_t *data, int len)
{
    for (int i=0; i<len; i++) {
        if ((i % 8) == 0)
            printf("\n%03d: ", i);
        printf("0x%02x ", data[i]);
    }
    printf("\n");
}

void
SepackLogger::process(uint8_t *data, uint8_t len)
{
    // printf("\n\n");
    // printf("mBuffer:");
    // printfbuf(mBuffer, mIdx);
    // printf("data:");
    // printfbuf(data, len);
    if (mIdx) {
        // printf("Copy %d bytes to Idx: %d\n", len, mIdx);
        memcpy(&mBuffer[mIdx], data, len);
        len += mIdx;
        data = (uint8_t *)mBuffer;
        // printf("new len: %d\n", len);
    }

    for (int i=0; i<len; i++) {
        uint32_t *mark = (uint32_t *)&data[i];
        if ((*mark >> 24) == 0xa5) {
            // printf("sync found at: %d\n", i);
            int bytesRead = tryProcessPacket((uint32_t *)&data[i], len - i);
            if (bytesRead <= 0) {
                mIdx = len - i;
                // printf("err: %d copy %d bytes\n", bytesRead, mIdx);
                memcpy(mBuffer, &data[i], mIdx);
                break;
            }
            mIdx = 0;
            i += bytesRead - 1;
        }
    }
}

void 
SepackLogger::parseDBfile()
{
    FILE *f;
    long len;
    char *data;
    cJSON *json;
    
    f = fopen ("/home/mg/prototyping/sepack-nxp/firmware/platform/lpc/debug_messages.json","rb");
    fseek (f,0,SEEK_END);
    len = ftell(f);
    fseek (f,0,SEEK_SET);
    data = (char*)malloc (len+1);
    len = fread (data, 1, len, f);
    fclose (f);
    
    json = cJSON_Parse(data);
    if (!json) {
        throw SepackLoggerError (std::string("cJSON: ") + cJSON_GetErrorPtr());
    }

    free(data);

    printf("Stype: %d\n", json->type);
    cJSON *c = json->child; 
    while (c) {
        char *msg = cJSON_GetObjectItem(json, c->string)->valuestring;
        // printf("type: %d: %s -> %s\n", c->type, c->string, msg);
        messages.insert (std::pair<uint32_t, std::string>(atoi(c->string), msg));
        c = c->next;    
    }
    cJSON_Delete(json);

    // std::map<uint32_t, std::string>::iterator it;
    // for (it=messages.begin(); it!=messages.end(); ++it)
        // printf("0x%08x -> %s\n", it->first, it->second.c_str());

}
