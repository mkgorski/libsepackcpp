#include <iostream>
#include <cstdio>
#include <cstring>
#include "sepack.h"

#define EP_IN  0x81
#define EP_OUT 0x01

using namespace std;

string const Sepack::defaultSerial = "AVRISP";

Sepack::Sepack ()
    : mEndpointIn(EP_IN),
      mEndpointOut(EP_OUT),
      mSepackHandle(NULL),
      mRxCb(NULL),
      mDbgRxCb(NULL),
      mRxThreadRunning(false),
      mSepackLogger(NULL)
{}

Sepack::~Sepack()
{
    deregisterRxCallback();

    if (mSepackHandle != NULL) {
        libusb_close (mSepackHandle);
        libusb_exit (NULL);
    }
    mSepackHandle = NULL;
}

void
Sepack::open (int vid, int pid, string serial)
{
    libusb_device **list;
    libusb_device_handle *handle = NULL;
    struct libusb_device_descriptor devDesc; 
    ssize_t count;
    char devSerial[64];
    int rc;

    if (!vid) vid = Sepack::defaultVid;
    if (!pid) pid = Sepack::defaultPid;
    if (serial == "") serial = Sepack::defaultSerial;

    rc = libusb_init (NULL);
    if (rc < 0)
        throw SepackOpenError (string("libusb_init: ") + libusb_error_name(rc));

    count = libusb_get_device_list (NULL, &list);
    for (ssize_t i=0; i<count; i++) {
        libusb_device *device = list[i];

        rc = libusb_get_device_descriptor (device, &devDesc);
        if (rc < 0)
            throw SepackOpenError (string("libusb_get_device_descriptor: ") + libusb_error_name(rc));

        if ((devDesc.idVendor == vid) && (devDesc.idProduct == pid)) {
            rc = libusb_open (device, &handle);
            if (rc < 0)
                throw SepackOpenError (string("libusb_open: ") + libusb_error_name(rc));

            rc = libusb_get_string_descriptor_ascii (handle, devDesc.iSerialNumber, (unsigned char *)devSerial, sizeof(devSerial));
            if (rc < 0)
                throw SepackOpenError (string("libusb_get_string_descriptor_ascii: ") + libusb_error_name(rc));

            if (!strncmp (devSerial, serial.c_str(), rc)) {
                if (libusb_kernel_driver_active (handle, 0)) {
                    rc = libusb_detach_kernel_driver (handle, 0);
                    if (rc < 0)
                        throw SepackOpenError (string("libusb_detach_kernel_driver: ") + libusb_error_name(rc));
                }

                rc = libusb_claim_interface (handle, 0);
                if (rc < 0)
                    throw SepackOpenError (string("libusb_claim_interface: ") + libusb_error_name(rc));

                mSepackHandle = handle;
                return;
            }
            libusb_close (handle);
        }
    }   

    libusb_free_device_list (list, 1);
    libusb_exit (NULL);
    throw SepackOpenError ("Device not found");
}

void
Sepack::close ()
{
    if (mSepackHandle != NULL) {
        libusb_close (mSepackHandle);
        libusb_exit (NULL);
    }
    mSepackHandle = NULL;
}

int 
Sepack::interruptRecv (unsigned char *inBuf, int size)
{
    return interruptTransfer (mEndpointIn, inBuf, size);
}

int 
Sepack::interruptSend (unsigned char *inBuf, int size)
{
    static unsigned char bbbuf[64] = {SEPACK_PID_USER, 0};

    size = (size > 60) ? 60 : size;
    bbbuf[3] = size;
    memcpy(&bbbuf[4], inBuf, size);
    return interruptTransfer (mEndpointOut, bbbuf, size + 4);
}

int 
Sepack::bulkTransfer (int endpoint, unsigned char *buf, int size)
{
    int nbytes;
    int rc;

    if (mSepackHandle == NULL)
        throw SepackError ("Cannot send data before opening the device!\n");

    rc = libusb_bulk_transfer (mSepackHandle, endpoint, buf, size, &nbytes, 1000);
    if (rc < 0) {
        if (rc == LIBUSB_ERROR_TIMEOUT)
            throw SepackTimeoutError ("libusb_bulk_transfer");
        else
            throw SepackError (string("bulkTransfer: ") + libusb_error_name(rc));
    }


    return nbytes;
}

int 
Sepack::interruptTransfer (int endpoint, unsigned char *buf, int size)
{
    int nbytes;
    int rc;

    if (mSepackHandle == NULL)
        throw SepackError ("Cannot send data before opening the device!\n");

    // TODO: Read EP_IN/EP_OUT from descriptors!
    rc = libusb_interrupt_transfer (mSepackHandle, endpoint, buf, size, &nbytes, 500);
    if (rc < 0) {
        if (rc == LIBUSB_ERROR_TIMEOUT)
            throw SepackTimeoutError ("libusb_interrupt_transfer");
        else
            throw SepackError (string("interruptTransfer: ") + libusb_error_name(rc));
    }

    return nbytes;
}

int 
Sepack::controlTransfer (unsigned char *buf, int size)
{
    int rc;

    if (mSepackHandle == NULL)
        throw SepackError ("Cannot send data before opening the device!\n");

    // TODO: Read EP_IN/EP_OUT from descriptors!
    rc = libusb_control_transfer (mSepackHandle, 
                                  (1<<7) | (1<<5) | (1<<0),
                                  0x01, //hid in report
                                  0x0101,
                                  0x0000,
                                  buf, size, 500);
    if (rc < 0) {
        if (rc == LIBUSB_ERROR_TIMEOUT)
            throw SepackTimeoutError ("libusb_interrupt_transfer");
        else
            throw SepackError (string("interruptTransfer: ") + libusb_error_name(rc));
    }

    return rc;
}

void 
Sepack::setMode(uint8_t mode)
{
    unsigned char buf[64] = {SEPACK_PID_CMD, SEPACK_CMD_SET_MODE, mode, 0};
    interruptTransfer(mEndpointOut, buf, sizeof(buf));
}

void 
Sepack::ioctl(int cmd, int arg)
{
    unsigned char buf[64] = {SEPACK_PID_CMD, 0, 0, 0};
    buf[1] = cmd; buf[2] = arg;
    interruptTransfer(mEndpointOut, buf, sizeof(buf));
}

void
Sepack::registerRxCallback(rx_cb_t cb)
{
    mRxCb = cb;
    if (!mRxThreadRunning) {
        startRxThread();
    }
}

void
Sepack::deregisterRxCallback()
{
    if (mRxCb)
        mRxCb = NULL;

    if (mRxThreadRunning)
        stopRxThread();
}

void
Sepack::registerDebugRxCallback(rx_cb_t cb)
{
    mDbgRxCb = cb;
    if (!mRxThreadRunning)
        startRxThread();
}

void
Sepack::deregisterDebugRxCallback()
{
    if (mDbgRxCb)
        mDbgRxCb = NULL;

    if (mRxThreadRunning)
        stopRxThread();
}

void
Sepack::enableStdoutDebug()
{
    if (!mSepackLogger) {
        mSepackLogger = new SepackLogger();

        if (!mRxThreadRunning)
            startRxThread();
    }
}

void
Sepack::disableStdoutDebug()
{
    if (mSepackLogger) {
        delete mSepackLogger;
        mSepackLogger = NULL;

        if (mRxThreadRunning)
            stopRxThread();
    }
}

void
Sepack::startRxThread()
{
    int err;
    pthread_attr_t attr;

    err = pthread_attr_init(&attr);
    if (err != 0)
        throw SepackThreadError(string("pthread_attr_init failed: ") + strerror(err));

    err = pthread_attr_setstacksize(&attr, 1024 * 1024);
    if (err != 0)
        throw SepackThreadError(string("pthread_attr_setstacksize failed: ") + strerror(err));

    err = pthread_create(&mRxThreadHandle, &attr, rxThread, this);
    if (err != 0)
        throw SepackThreadError(string("pthread_create failed: ") + strerror(err));

    mRxThreadRunning = true;
}

void
Sepack::stopRxThread()
{
    mRxThreadRunning = false;
    pthread_join(mRxThreadHandle, NULL);
}

void *
Sepack::rxThread(void *arg)
{
    Sepack *s = (Sepack *)arg;
    uint8_t data[64];
    int nbytes;

    while (s->mRxThreadRunning) {
        try {
            nbytes = s->interruptRecv(data, 64);
        }
        catch (SepackTimeoutError &e) {
            cout << "Timeout occured" << endl;
            continue;
        }
        catch (SepackError &e) {
            cout << "Exception: " << e.msg << endl;
            s->mRxThreadRunning = false;
            break;
        }
        s->handleRxPacket(data, nbytes);
    }

    return NULL;
}

void
Sepack::handleRxPacket(uint8_t *data, int len)
{
    if (len != 64) {
        cout << "handleRxPacket: wrong packet length, expected 64 got %d" << len << endl;
        return;
    }

    uint8_t datalen = data[3];
    switch (data[0]) {
        case SEPACK_PID_USER:
            if (mRxCb)
                mRxCb(&data[4], datalen);
            break;
        case SEPACK_PID_DBG:
            if (mSepackLogger)
                mSepackLogger->process(&data[4], datalen);

            if (mDbgRxCb)
                mDbgRxCb(&data[4], datalen);
            break;
        case SEPACK_PID_CMD:
        case SEPACK_PID_NONE:
        default:
            break;
    }
}
