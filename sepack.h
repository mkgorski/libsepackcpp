#ifndef SEPACK_H
#define SEPACK_H

#include <string>
#include <libusb-1.0/libusb.h>
#include <pthread.h>
#include "sepack_logger.h"

#define SEPACK_PID_NONE 0x00
#define SEPACK_PID_USER 0x01
#define SEPACK_PID_CMD  0x02
#define SEPACK_PID_DBG  0x03

#define SEPACK_CMD_SET_MODE    0x01
#define SEPACK_CMD_CUSTOM_MIN  0x02

typedef void (*rx_cb_t)(uint8_t *, int);

class Sepack {
  public:
    Sepack ();
    ~Sepack();

    void open (int vid = 0, int pid = 0, std::string serial = "");
    void close ();

    int interruptSend (unsigned char *outBuf, int size);
    int interruptRecv (unsigned char *inBuf, int size);
    int controlTransfer (unsigned char *buf, int size);
    void setMode(uint8_t mode);
    void ioctl(int cmd, int arg);
    void registerRxCallback(rx_cb_t cb);
    void deregisterRxCallback();
    void registerDebugRxCallback(rx_cb_t cb);
    void deregisterDebugRxCallback();
    void enableStdoutDebug();
    void disableStdoutDebug();
    int  bulkTransfer (int endpoint, unsigned char *buf, int size);

  private:
    static void *rxThread(void *arg);
    int interruptTransfer (int endpoint, unsigned char *buf, int size);
    void handleRxPacket(uint8_t *data, int len);
    void startRxThread();
    void stopRxThread();

    int mEndpointIn;
    int mEndpointOut;
    int mEndpointSize;
    libusb_device_handle *mSepackHandle;
    rx_cb_t mRxCb;
    rx_cb_t mDbgRxCb;
    bool mRxThreadRunning;
    pthread_t mRxThreadHandle;
    SepackLogger *mSepackLogger;
    static int const defaultVid = 0x16d0;
    static int const defaultPid = 0x0450;
    static std::string const defaultSerial;
};

class SepackError {
  public:
    SepackError(std::string msg) {
      this->msg = msg;
    }
    std::string msg;
};

class SepackOpenError : public SepackError {
  public:
    SepackOpenError(std::string msg) 
    : SepackError(msg) 
    {}
};

class SepackTimeoutError : public SepackError {
  public:
    SepackTimeoutError(std::string msg) 
    : SepackError(msg)
    {}
};

class SepackThreadError : public SepackError {
  public:
    SepackThreadError(std::string msg)
    : SepackError(msg)
    {}
};

class SepackLoggerError : public SepackError {
  public:
    SepackLoggerError(std::string msg)
    : SepackError(msg)
    {}
};

#endif /* SEPACK_H */
